# `bb`

`bb`, or **_b_**etter **_b_**ackup, is a DevOps tool to backup, deploy, build, and restore web applications. It is built using [rust](rust-lang.org) and runs on Mac, Linux, and Windows.

## system requirements

- `git` - `bb` will use the `git` executable installed on the system to run.
- `tar` - `bb` will use `tar` to archive the current state of the repo folder
  - windows alternative: if on windows, we'll try to use `tar`, `bsdtar`, and `7z`
- `rsync` - `bb` will use `rsync` to transfer `tar` files biderectionally between the backup and project folders.
  - if `rsync` isn't installed on the system, we'll try to use `cp` (linux and windows).

## usage

1. In the parent directory of your `repo` folder, run `bb init` to initialize the `.bb.json`. This will create the configuration for this project.

## development

### rust toolkit

Using `cargo`, you can build and run the app locally. Executing `cargo run -- deploy main` will deploy the main branch.

### building rust on mac

You can build for all major architectures on your mac! I'm currently targeting these operating systems and architectures:

1. `aarch64-apple-darwin` - this binary is for M1 macs.
2. `x86_64-apple-darwin` - this binary is for intel based macs
3. `aarch64-unknown-linux-musl` - this bin is for arm64 Linux
4. `x86_64-unknown-linux-musl` - this binary is for amd64 Linux. _note:_ MSU's Linux servers will probably use this binary.
5. `x86_64-pc-windows-gnu` - this binary is for windows. _note:_ MSU's windows servers will probably use this binary

You can install the needed rust toolchains and build the production-ready code by running the bash script `build.sh` in the repo's main directory.

### `build.sh` dependencies:

- tested on mac
- `rust` needs to be installed: https://www.rust-lang.org/learn/get-started
- `brew` needs to be installed: https://brew.sh
- `x86_64-w64-mingw32-gcc` needs to be installed: `brew install x86_64-w64-mingw32-gcc`
