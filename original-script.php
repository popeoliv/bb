#!/usr/bin/env php
<?php

/**
 * bb is a cli tool designed to backup and restore your site using rsync
 * @author Oliver Pope <popeoliv@msu.edu>
 * @version 1.0.0
 */

echo "[BB Backup/Restore 1.0.0]" . PHP_EOL;
$config_ini = __DIR__ . DIRECTORY_SEPARATOR . 'config.ini';


/**
 * prints line with prefix and suffix
 * @param mixed $args
 * @return void
 */
function print_line(...$args)
{
    echo "[BB] " . implode(" ", $args) . PHP_EOL;
}

/**
 * Handles error and kills the app
 * @param String $error Error
 * @param String $message More details on error
 * @return never
 */
function handle_error($error, $message)
{
    print_line("[ERROR]", $error);
    print_line($message);
    exit(1);
}

function handle_warning($warning, $message)
{
    print_line("[WARN]", $warning);
    print_line($message);
}

function validate_env($env)
{
    if (!isset($env['PROJECT_BASE_PATH'])) {
        handle_error("invalid config.ini", "PROJECT_BASE_PATH not found");
    }
    if (!isset($env['BACKUP_BASE_PATH'])) {
        handle_error("invalid config.ini", "BACKUP_BASE_PATH not found");
    }
    if (!isset($env['REPO_DIRECTORY_NAME'])) {
        handle_warning("invalid config.ini", "REPO_DIRECTORY_NAME not found, defaulting to 'repo'");
    }
    if (!isset($env['BACKUP_DIRECTORY_NAME'])) {
        handle_warning("invalid config.ini", "BACKUP_DIRECTORY_NAME not found, defaulting to 'backup'");
    }
}

function migrateSQL($db, $host) {
    return <<<MSQL
UPDATE {$db->prefix}_2_options t
SET t.option_value = 'https://internal.$host'
WHERE t.option_id = 2;
UPDATE {$db->prefix}_2_options t
SET t.option_value = 'https://internal.$host'
WHERE t.option_id = 1;
UPDATE {$db->prefix}_blogs t
SET t.domain = '$host'
WHERE t.blog_id = 1;
UPDATE {$db->prefix}_blogs t
SET t.domain = 'internal.$host'
WHERE t.blog_id = 2;
UPDATE {$db->prefix}_options t
SET t.option_value = 'https://$host'
WHERE t.option_id = 1;
UPDATE {$db->prefix}_options t
SET t.option_value = 'https://$host'
WHERE t.option_id = 2;
UPDATE {$db->prefix}_site t
SET t.domain = '$host';
MSQL;
};

function reconnect_dir($orig_repo, $target_repo, $tmp_repo, $project_base_path, $persisted) {
    if(file_exists($orig_repo)) {
        system("rm -rf $orig_repo");
    }
    echo "mv $target_repo $orig_repo" . PHP_EOL;
    echo "mv $tmp_repo $target_repo" . PHP_EOL;

    system("mv $target_repo $orig_repo");
    system("mv $tmp_repo $target_repo");

    // fix permissions
    system("chmod -R 2775 $target_repo");

    // relink repo to persisted folders
    foreach ($persisted as list($after_composer, $root, $target)) {
        if ($after_composer == "0") {
            system("rm -rf " . $target_repo . DIRECTORY_SEPARATOR . $target);
            system("ln -s  " . $project_base_path . DIRECTORY_SEPARATOR . $root . " " . $target_repo . DIRECTORY_SEPARATOR . $target);
        }
    }

    system("cd $target_repo && scl enable php74 '../composer.phar install --no-ansi --no-dev --no-interaction --optimize-autoloader'");

    foreach ($persisted as list($after_composer, $root, $target)) {
        if ($after_composer == "1") {
            system("rm -rf " . $target_repo . DIRECTORY_SEPARATOR . $target);
            system("ln -s  " . $project_base_path . DIRECTORY_SEPARATOR . $root . " " . $target_repo . DIRECTORY_SEPARATOR . $target);
        }
    }
}

if (!file_exists($config_ini)) {
    handle_error("config.ini not found", "please run command in the same directory as your config.ini.");
}

if ($argc < 2) {
    handle_error("not enough arguments", "usage: bb {b,r,p}");
}

$env = parse_ini_file($config_ini);

validate_env($env);

// CONFIG.INI PARAMS
$project_base_path =  $env['PROJECT_BASE_PATH'];
$repo_dir_name = $env['REPO_DIRECTORY_NAME'] ?? "repo";
$backup_base_path = $env['BACKUP_BASE_PATH'];
$backup_directory_name = $env['BACKUP_DIRECTORY_NAME'] ?? "backup";
$exclusions = $env["BB_EXCLUDE"] ?? [];
$persisted = array_map(function ($row) {
    return explode(",", $row);
}, $env["BB_PERSIST"] ?? []);

if (isset($env["DB_HOST"], $env["DB_USER"], $env["DB_PASSWORD"], $env["DB_HOST"])) {
    $db = new stdClass();
    $db->name = $env["DB_NAME"];
    $db->user = $env["DB_USER"];
    $db->pass = $env["DB_PASSWORD"];
    $db->host = $env["DB_HOST"];
    $db->ssl = isset($env["USE_SSL"]) && in_array($env["USE_SSL"], ['1', 1, true, 'TRUE', 'true', 'yes', 'YES']);
    $db->pem = $env["MYSQL_SSL_PEM_FILE"] ?? null;
    $db->prefix = $env["DB_PREFIX"] ?? "wp";
} else {
    handle_error("invalid config.ini", "DB_NAME|DB_USER|DB_PASSWORD|DB_HOST not found");
}

if (!file_exists($project_base_path)) {
    handle_error("Path doesn't exist", "project base path must exist");
} else if (!file_exists($project_base_path . DIRECTORY_SEPARATOR . $repo_dir_name)) {
    handle_error("Path doesn't exist", "project repo path must exist");
}

if (!file_exists($backup_base_path)) {
    handle_error("Path doesn't exist", "backup base path must exist");
}

$bb_folder = $project_base_path . DIRECTORY_SEPARATOR . '.bb';
$live_folder = $backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name . DIRECTORY_SEPARATOR . "live";

if (strncasecmp($argv[1], "b", 1) == 0) {
    print_line("[B]", "Starting Backup");

    // prepare destination for new backup
    // check if backup directory exists
    if (!file_exists($backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name)) {
        mkdir($backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name);
    }
    if (!file_exists($backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name . DIRECTORY_SEPARATOR . "snapshots")) {
        mkdir($backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name . DIRECTORY_SEPARATOR . "snapshots");
    }
    if (!file_exists($backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name . DIRECTORY_SEPARATOR . "live")) {
        mkdir($backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name . DIRECTORY_SEPARATOR . "live");
    }

    // okay, backup dir structure is what we need, now we need to begin the backup process
    // first backup repo folder to live/repo
    $excl = implode(" ", array_map(function ($exclude) {
        return "--exclude='$exclude'";
    }, $exclusions));
    $src = $project_base_path . DIRECTORY_SEPARATOR . $repo_dir_name;
    $dest = $backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name . DIRECTORY_SEPARATOR . "live";
    $rsync = escapeshellcmd("rsync -rlDP --info=progress2 $excl $src $dest");
    system($rsync);

    // next download sql then rsync
    $tmp = $project_base_path . DIRECTORY_SEPARATOR . "tmp";
    if (file_exists($tmp)) {
        rmdir($tmp);
    } else {
        mkdir($tmp);
    }
    $tmp_file = $tmp . DIRECTORY_SEPARATOR . "archive.sql";
    $ssl = ($db->ssl) ? "--ssl" : "";
    $pem = (!is_null($db->pem)) ? "--ssl-ca=$db->pem" : "";
    $mysqldump = escapeshellcmd("mysqldump --user=$db->name --password=$db->pass --host=$db->host $ssl $pem $db->name --result-file=$tmp_file");
    system($mysqldump);

    $dest_sql = $backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name . DIRECTORY_SEPARATOR . "live" . DIRECTORY_SEPARATOR . "archive.sql";

    $rsync_sql = escapeshellcmd("rsync -rlDP --info=progress2 $tmp_file $dest_sql");
    system($rsync_sql);

    if (file_exists($tmp)) {
        system("rm -rf $tmp");
    } else {
        mkdir($tmp);
    }

    $id = bin2hex(random_bytes(5));

    // finalize by tarring the whole of live and adding it to the snapshots folder
    $name = date('Y-m-d-H-i-s') . '-' . $id . ".tar.gz";
    $full_snapshot_path = $backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name . DIRECTORY_SEPARATOR . "snapshots" . DIRECTORY_SEPARATOR . $name;

    $tar = escapeshellcmd("tar -czf $full_snapshot_path $live_folder");
    system($tar);

    if (!file_exists($bb_folder)) {
        mkdir($bb_folder);
    }
    file_put_contents($bb_folder . DIRECTORY_SEPARATOR . 'backups', $name . PHP_EOL, FILE_APPEND | LOCK_EX);

    print_line("[B]", "Ending Backup");
} else if (strncasecmp($argv[1], "r", 1) == 0) {
    print_line("[R]", "Starting Restore");

    $backup_path = $backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name;

    if (!file_exists($bb_folder . DIRECTORY_SEPARATOR . 'backups')) {
        handle_error("Don't know restore history", $bb_folder . 'backups' . " does not exist");
    }
    if (!file_exists($backup_path)) {
        handle_error("Can't restore nothing!", $backup_path . " does not exist");
    }
    if (!file_exists($backup_path . DIRECTORY_SEPARATOR . "snapshots")) {
        handle_error("Can't find snapshot folder!", $backup_path . " does not contain a snapshots folder. Please back something up to restore");
    }
    if (!file_exists($backup_path . DIRECTORY_SEPARATOR . "live")) {
        handle_error("Can't find live folder!", $backup_path . " does not contain a live folder. Please back something up to restore");
    }

    $restore_idx = 0;
    if (isset($argv[2]) && is_numeric($argv[2])) {
        // restore based on argv
        $restore_idx = 0 + $argv[2];
    } else {
        handle_warning("Restoring latest restore_idx=0", "argv[2] defines the restore_idx to use, defaulting to 0");
    }

    // get filename of tarball, where the latest is first
    $backups = array_reverse(file($project_base_path . DIRECTORY_SEPARATOR . '.bb' . DIRECTORY_SEPARATOR . 'backups', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
    $snapshot_path = $backup_path . DIRECTORY_SEPARATOR . "snapshots" . DIRECTORY_SEPARATOR . $backups[$restore_idx];
    // restore the tarball
    if (file_exists($snapshot_path)) {

        // send the snapshot to the project base path
        $tmp_tarball = $project_base_path . DIRECTORY_SEPARATOR . '.bb' . DIRECTORY_SEPARATOR . "archive.tar.gz";
        $rsync_sql = escapeshellcmd("rsync -rlDP --info=progress2 $snapshot_path $tmp_tarball");
        system($rsync_sql);

        // decompress tarball
        $untar = escapeshellcmd("tar -xzf $tmp_tarball -C $bb_folder");
        system($untar);

        // restore sql
        $tmp_sql = $bb_folder . $live_folder . DIRECTORY_SEPARATOR . "archive.sql";
        $ssl = ($db->ssl) ? "--ssl" : "";
        $pem = (!is_null($db->pem)) ? "--ssl-ca=$db->pem" : "";
        $mysqlrestore = escapeshellcmd("mysql --user=$db->name --password=$db->pass --host=$db->host $ssl $pem $db->name");
        system("$mysqlrestore < $tmp_sql");

        // move the repo in place
        $tmp_repo = $bb_folder . $live_folder . DIRECTORY_SEPARATOR . $repo_dir_name;

        $target_repo = $project_base_path . DIRECTORY_SEPARATOR . $repo_dir_name;
        $orig_repo    = $project_base_path . DIRECTORY_SEPARATOR . $repo_dir_name . ".orig";

        reconnect_dir($orig_repo, $target_repo, $tmp_repo, $project_base_path, $persisted);

        // delete tmp file
        if (file_exists($tmp_tarball)) {
            unlink($tmp_tarball);
        }

        print_line("[R]", "Ending Restore");
    } else {
        handle_error("snapshot not in backups folder", $snapshot_path . " was not found, delete the index from .bb/backups and please don't delete any backups");
    }
} else if (strncasecmp($argv[1], "d", 1) == 0) {
    $branch_name    =  $argv[2] ?? "main";
    $target_repo    =  $project_base_path . DIRECTORY_SEPARATOR . $repo_dir_name;
    $tmp_repo       =  $target_repo . ".tmp.deploy";
    $orig_repo      =  $target_repo . ".orig.deploy";
    $repo_url       =  $env['GIT_REPOSITORY_URL'];

    if(!isset($repo_url)) {
        handle_error('no git repo url found', 'we need to know what to clone!');
    }

    system("git clone -b $branch_name --depth 1 $repo_url $tmp_repo");
    system("rm -rf $tmp_repo" . DIRECTORY_SEPARATOR . ".git");

    reconnect_dir($orig_repo, $target_repo, $tmp_repo, $project_base_path, $persisted);

    $theme = $target_repo . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "themes" . DIRECTORY_SEPARATOR . "EliBroad";
    system("cd $theme && npm i && npm run next:build");

    print_line("[R]", "Ending Deployment. Please run 'bb b' to make a backup");
} else if(strncasecmp($argv[1], "sp", 2) == 0) {
    // synchronize production
    $tmp_sync_dir = $project_base_path . DIRECTORY_SEPARATOR . ".tmp_sync";

    if(file_exists($tmp_sync_dir)) {
        system("rm -rf $tmp_sync_dir");
    }

    system("mkdir $tmp_sync_dir");

    // sync uploads tmp_sync_dir
    $uploads_dir = $project_base_path . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;
    $rsync_sql = escapeshellcmd("rsync -rlDP --info=progress2 $uploads_dir $tmp_sync_dir/uploads");

    $sql_file = $tmp_sync_dir . DIRECTORY_SEPARATOR . "local.sql";
    $sql_file_test = $tmp_sync_dir . DIRECTORY_SEPARATOR . "test2.sql";
    $sql_file_dev = $tmp_sync_dir . DIRECTORY_SEPARATOR . "dev2.sql";

    $ssl = ($db->ssl) ? "--ssl" : "";
    $pem = (!is_null($db->pem)) ? "--ssl-ca=$db->pem" : "";

    $dumpSchema = escapeshellcmd("mysqldump --user=$db->name --password=$db->pass --host=$db->host $ssl $pem --no-data $db->name");
    system("$dumpSchema > $sql_file");
    system("$dumpSchema > $sql_file_test");
    system("$dumpSchema > $sql_file_dev");

    $base_cmd = "mysqldump --user=$db->name --password=$db->pass --host=$db->host $ssl $pem --no-create-info";
    $dumpDB = escapeshellcmd("$base_cmd --ignore-table=$db->name.{$db->prefix}_users --ignore-table=$db->name.{$db->prefix}_usersmeta $db->name");
    system("$dumpDB >> $sql_file");

    $dumpDBDevTest = escapeshellcmd("$base_cmd $db->name");
    system("$dumpDBDevTest >> $sql_file_test");
    system("$dumpDBDevTest >> $sql_file_dev");

    $user_table = "{$db->prefix}_users";
    $usermeta_table = "{$db->prefix}_users";

    $localUser = <<<SQL
INSERT INTO $user_table ('ID', 'user_login', 'user_pass', 'user_nicename', 'user_email', 'user_url', 'user_registered', 'user_activation_key', 'user_status', 'display_name') VALUES ('1', 'admin', MD5('admin'), 'Sparty', 'sparty@broad.msu.edu', 'https://broad.msu.edu/', '2011-06-07 00:00:00', '', '0', 'Sparty');
INSERT INTO $usermeta_table ('umeta_id', 'user_id', 'meta_key', 'meta_value') VALUES (NULL, '1', 'wp_capabilities', 'a:1:{s:13:"administrator";s:1:"1";}');
INSERT INTO $usermeta_table ('umeta_id', 'user_id', 'meta_key', 'meta_value') VALUES (NULL, '1', 'wp_user_level', '10');
SQL;
    file_put_contents($sql_file, $localUser . PHP_EOL, FILE_APPEND | LOCK_EX);

    $migrationScriptLocal = migrateSQL($db, "broad.test");
    $migrationScriptDev = migrateSQL($db, "dev2.broad.msu.edu");
    $migrationScriptTest = migrateSQL($db, "test2.broad.msu.edu");
    file_put_contents($sql_file, $migrationScriptLocal . PHP_EOL, FILE_APPEND | LOCK_EX);
    file_put_contents($sql_file_dev, $migrationScriptDev . PHP_EOL, FILE_APPEND | LOCK_EX);
    file_put_contents($sql_file_test, $migrationScriptTest . PHP_EOL, FILE_APPEND | LOCK_EX);

    $tarAndZip = $project_base_path . DIRECTORY_SEPARATOR . "production-sync.tar.gz";
    system("tar -czf $tarAndZip $tmp_sync_dir/");

    $backupsSync = $backup_base_path . DIRECTORY_SEPARATOR . $backup_directory_name . DIRECTORY_SEPARATOR . "sync";
    $rsync_sql = escapeshellcmd("rsync -rlDP --info=progress2 $tarAndZip $backupsSync");

} else if(strncasecmp($argv[1], "sd", 2) == 0) {
    // synchronize dev environments


} else {
    handle_error("invalid argument", "valid arguments are b, r, and d\nRevert: bb r n (n is an integer between 0 and the length of the snapshot index - 1)\nDeploy: bb d branch_name\nBackup: bb b");
}

exit(0);
