use crate::helpers::{
    self, bb_set_permissions, create_dir_if_not_exists, join_paths, message, run_rsync_or_copy,
    CommandType,
};
use crate::hooks::{Hook, HookFunctions};
use crate::settings::SettingsSchema;
use anyhow::{Ok, Result};
use console::style;
use std::collections::HashMap;

use std::path::PathBuf;

/**
 * Run the backup algorithm. Copies repository to backups/live, then tars backups/live, taking a sha256 checksum of the tar file.
 * finally compresses the tar file and saves it to the backups/archived folder.
 */
pub fn backup(settings: &SettingsSchema, hooks: &HashMap<&'static str, Hook>) -> Result<()> {
    message(CommandType::BACKUP, String::from("backing up repo"));

    // build path live folder
    let backup_live_folder = join_paths(&settings.backup_path, &PathBuf::from("live"));

    // make sure backup folder exists.
    create_dir_if_not_exists(&backup_live_folder)?;

    // set permissions only if in unix-land
    bb_set_permissions(&backup_live_folder, true)?;

    // make sure archives folder exists.
    let backup_archived_folder = join_paths(&settings.backup_path, &PathBuf::from("archived"));

    create_dir_if_not_exists(&backup_archived_folder)?;

    // set permissions only if in unix-land
    bb_set_permissions(&backup_archived_folder, true)?;

    // we should only do this if the repo exists, otherwise lets not backup nothing.
    let project_repo_dir = join_paths(&settings.project_base_path, &settings.repo_directory_name);

    // ensure project dir exists
    if !project_repo_dir.exists() {
        message(
            CommandType::BACKUP,
            format!(
                "{} not in {}. skipping backup.",
                style(settings.repo_directory_name.display()).blue(),
                style(settings.project_base_path.display()).magenta()
            ),
        );

        return Ok(());
    }

    hooks["pre_backup"].run_hook()?;

    message(
        CommandType::BACKUP,
        format!(
            "copying {} to {}",
            style(&project_repo_dir.display()).bold().yellow(),
            style(&backup_live_folder.display()).bold().green()
        ),
    );

    // copy repo folder to live backup using rsync (if available)
    run_rsync_or_copy(&project_repo_dir, &backup_live_folder)?;

    message(
        CommandType::BACKUP,
        format!(
            "snapshotting {} with tar",
            style(&backup_live_folder.display()).bold().green()
        ),
    );

    let tmp_backup_dir = join_paths(&settings.backup_path, &PathBuf::from(".bb"));
    create_dir_if_not_exists(&tmp_backup_dir)?;

    let (checksum, checksum_bytes, compressed_bytes) = helpers::tar_and_deflate(
        &backup_live_folder,
        &backup_archived_folder,
        &tmp_backup_dir,
    )?;

    message(
        CommandType::BACKUP,
        format!(
            "{} written to sha256 checksum. {} compressed. Wrote {}.tgz",
            indicatif::HumanBytes(checksum_bytes),
            indicatif::HumanBytes(compressed_bytes),
            checksum
        ),
    );

    message(
        CommandType::BACKUP,
        String::from("backup saved to archives"),
    );

    hooks["post_backup"].run_hook()?;
    Ok(())
}
