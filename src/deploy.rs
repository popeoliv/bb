use anyhow::{Context, Result};
use std::{
    collections::HashMap,
    fs::{create_dir, remove_dir_all, rename},
    path::{Path, PathBuf},
    process::Command,
};

use console::style;

use crate::{
    helpers::{bb_set_permissions, join_paths, message},
    hooks::{Hook, HookFunctions},
    settings::SettingsSchema,
};

/**
 * Runs the main deployment script.
 * 1. Clones a repository to ".bb/tmp"
 * 2. Runs build hook
 * 3. Removes project's repo folder
 * 4. Renames .bb/tmp to the specified repo folder.
 */
pub fn deploy(
    settings: &SettingsSchema,
    hooks: &HashMap<&'static str, Hook>,
    branch: &String,
) -> Result<()> {
    hooks["pre_deploy"].run_hook()?;

    let tmp_repo_path = Path::new(".bb/tmp");
    if tmp_repo_path.exists() {
        remove_dir_all(tmp_repo_path)?;
    }

    hooks["pre_pull"].run_hook()?;

    message(
        crate::helpers::CommandType::DEPLOY,
        String::from("starting git clone"),
    );

    // run git. fail if git isn't detected
    let git_bin = which::which("git").context("git is not installed on this system.")?;

    Command::new(git_bin)
        .args([
            "clone",
            "--depth",
            "1",
            "-b",
            branch,
            settings.git_repository_url.as_str(),
            ".bb/tmp",
        ])
        .spawn()?
        .wait()?;

    bb_set_permissions(&PathBuf::from(".bb/tmp"), true)?;

    // finalized clone.
    message(
        crate::helpers::CommandType::DEPLOY,
        String::from("clone finished"),
    );

    // now delete .git folder so we dont confuse 'em
    remove_dir_all(Path::new(".bb/tmp/.git"))?;

    // run post script
    hooks["post_pull"].run_hook()?;

    // run build script if exists
    hooks["build"].run_hook()?;

    // run link
    hooks["pre_link"].run_hook()?;

    // get project repo directory
    let project_repo_dir = join_paths(&settings.project_base_path, &settings.repo_directory_name);

    // check if project dir already exists
    if project_repo_dir.exists() {
        message(
            crate::helpers::CommandType::DEPLOY,
            String::from("deleting old repo"),
        );

        remove_dir_all(&project_repo_dir)
            .context("deployment failed! couldn't delete project repo directory.")?;
    }

    create_dir(&project_repo_dir).context("deployment failed! couldn't create repo directory")?;

    bb_set_permissions(&project_repo_dir, true)?;

    message(
        crate::helpers::CommandType::DEPLOY,
        format!(
            "renaming .bb/tmp to {}",
            style(project_repo_dir.display()).cyan()
        ),
    );

    rename(".bb/tmp", project_repo_dir)?;

    hooks["post_link"].run_hook()?;
    hooks["post_deploy"].run_hook()?;

    Ok(())
}
