mod backup;
mod deploy;
mod helpers;
pub mod hooks;
mod restore;
pub mod settings;
mod validate;
mod initialize;

use crate::{deploy::deploy, restore::restore, validate::validate};
use anyhow::{Ok, Result};
use backup::backup;
use clap::{Parser, Subcommand};
use console::{style};
use ctrlc::set_handler;
use helpers::message;
use schemars::schema_for;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[arg(short, long)]
    verbose: Option<bool>,

    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// helper to initialize bb
    Init {
        /// use default parameters to initialize .bb.json
        #[arg(short, long)]
        yes: Option<bool>,
    },

    /// deploy branch from a git repository
    Deploy {
        branch: String,

        #[arg(short, long)]
        backup_only: Option<bool>,
    },

    /// restore from latest backup
    Restore {},

    /// generate jsonschema
    Schema {},
}

fn main() -> Result<()> {
    let cli = Cli::parse();

    set_handler(|| {
        // this handler gets called on linux OS interupts: SIGINT, SIGTERM, SIGHUP
        // on windows, CTRL_C_EVENT or CTRL_BREAK_EVENT
        println!("Ctrl+C called before bb was finished running! (not quitting yet).");
    })?;

    std::env::set_var(
        "VERBOSE_MODE",
        if cli.verbose.unwrap_or(false) {
            "1"
        } else {
            "0"
        },
    );

    match &cli.command {
        Commands::Deploy {
            branch,
            backup_only,
        } => {
            message(helpers::CommandType::DEPLOY, format!("Deploying branch {}.", style(branch).bold()));

            // call the validate function and return an error if it fails
            let validated_return = validate()?;

            // call the backup function with the settings and hooks from the validated return, and return an error if it fails
            backup(&validated_return.settings, &validated_return.hooks)?;

            // if the backup_only variable is not none and its unwrapped value is true
            if !backup_only.is_none() && backup_only.unwrap() {
                message(
                    helpers::CommandType::DEPLOY,
                    format!("{}", style("backup only specified, not deploying.")),
                );
                return Ok(()); // return an Ok result to indicate success
            }

             // call the deploy function with the settings, hooks, and branch, and return an error if it fails
            deploy(&validated_return.settings, &validated_return.hooks, branch)?;
        }

        Commands::Restore {} => {
            let validated_return = validate()?;
            restore(&validated_return.settings, &validated_return.hooks)?;
        }

        Commands::Schema {} => {
            let schema = schema_for!(settings::SettingsSchema);
            println!("{}", serde_json::to_string_pretty(&schema).unwrap());
        }

        Commands::Init { yes } => {
            // check for .bb.json
            initialize::initialize(yes.unwrap_or(false))?;
        }
    }

    Ok(())
}
