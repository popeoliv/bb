use std::{
    cmp::Ordering,
    collections::HashMap,
    fs::{remove_dir_all, rename},
    path::PathBuf,
};

use anyhow::{Context, Ok, Result};
use console::style;
use dialoguer::{Confirm, Select};
use time::OffsetDateTime;
use walkdir::{DirEntry, WalkDir};

use crate::{
    helpers::{
        bb_set_permissions, create_dir_if_not_exists, join_paths, message, run_rsync_or_copy,
        untar_and_inflate, CommandType,
    },
    hooks::{Hook, HookFunctions},
    settings::SettingsSchema,
};

/**
 * compare the modification times of two DirEntry objects and return the Ordering
 */
fn order_by_modified(a: &DirEntry, b: &DirEntry) -> Result<Ordering> {
    let a_modified = a.metadata()?.modified()?;
    let b_modified = b.metadata()?.modified()?;

    // compare the modification times and get the Ordering
    let ordering = b_modified.cmp(&a_modified);

    Ok(ordering)
}

// determine if a DirEntry is hidden
fn is_hidden(entry: &DirEntry) -> bool {
    // get the filename as a string and check if it starts with a "."
    entry
        .file_name()
        .to_str()
        .map(|s| s.starts_with("."))
        .unwrap_or(false)
}

/**
 * select a file from a specified directory and return its path
 */
fn select(archive_path: &PathBuf) -> Result<PathBuf> {

     // create an iterator to walk through the specified directory
    let walking_dir_iter = WalkDir::new(archive_path)
        .max_depth(1) // only search one directory deep
        .sort_by(|a, b| order_by_modified(a, b).expect("couldn't order modified dates!")) // sort by the modification time
        .into_iter()
        .filter_map(|e| e.ok()) // filter out any errors
        .filter(|e| e.file_type().is_file() && !is_hidden(e)); // only include files that are not hidden

        let mut direntry_vector: Vec<DirEntry> = vec![]; // create a vector for the DirEntry objects
        let mut matches_vector: Vec<String> = vec![]; // create a vector for the matching strings

        for (index, entry) in walking_dir_iter.enumerate() {
            matches_vector.push(format!(
                "{}: {} - {}",
                index,
                OffsetDateTime::from(entry.metadata()?.modified()?)
                    .format(&time::format_description::well_known::Rfc2822)?,
                entry.file_name().to_str().unwrap_or("filename not found")
            ));

            // add the entry to the vector of DirEntry objects
            direntry_vector.push(entry);
        }

        // create a new selection prompt
        let selection = Select::new()
            .items(&matches_vector) // specify the options
            .default(0)
            .with_prompt("Restore from which backup? (last modified first)") // set the prompt text
            .max_length(10) // set the maximum length for the options
            .interact()?; // get the user's selection

        // return the path of the selected file
        Ok(direntry_vector[selection].clone().into_path())
}


/**
 * restore the project from a backup, applying any defined hooks
 */
pub fn restore(settings: &SettingsSchema, hooks: &HashMap<&'static str, Hook>) -> Result<()> {
    message(
        crate::helpers::CommandType::RESTORE,
        String::from("beginning restore..."),
    );

    // begin restore process
    hooks["pre_restore"].run_hook()?;

    let archive_path = join_paths(&settings.backup_path, &PathBuf::from("archived"));

    let archive_to_restore = select(&archive_path)?;

    let tmp_backup_dir = join_paths(&settings.backup_path, &PathBuf::from(".bb"));
    create_dir_if_not_exists(&tmp_backup_dir)?;
    bb_set_permissions(&tmp_backup_dir, false)?;

    let tmp_restore_point = join_paths(&settings.backup_path, &PathBuf::from(".bb/restore"));
    create_dir_if_not_exists(&tmp_restore_point)?;
    bb_set_permissions(&tmp_restore_point, false)?;

    let (checksum, checksum_bytes, decompressed_bytes) =
        untar_and_inflate(&archive_to_restore, &tmp_restore_point, &tmp_backup_dir)?;

    message(
        crate::helpers::CommandType::RESTORE,
        format!(
            "deflated and calculated checksum of {}. Deflated {} and calculated checksum of {}",
            style(&checksum).bold().magenta(),
            style(indicatif::HumanBytes(decompressed_bytes)).bold(),
            style(indicatif::HumanBytes(checksum_bytes)).bold()
        ),
    );

    // get the original checksum from filename
    let binding = archive_to_restore.clone();
    let original_checksum = binding
        .file_name()
        .unwrap_or_default()
        .to_str()
        .unwrap_or_default()
        .strip_suffix(".tgz")
        .unwrap();

    // fail out if checksums do not match and user confirms end.
    if original_checksum != checksum {
        if Confirm::new()
            .with_prompt(
                "hashes do not match, validation of decompressed tar failed! Continue anyway?",
            )
            .interact()?
        {
            message(
                CommandType::RESTORE,
                format!(
                    "forcing restore with unmatched checksums ({})",
                    style("risky").red().bold()
                ),
            );
        } else {
            message(CommandType::RESTORE, String::from("restore ended early"));
            return Ok(());
        }
    }

    let tmp_repo_dir = join_paths(&tmp_restore_point, &settings.repo_directory_name);
    let quick_backup = PathBuf::from(".bb/quick_backup");

    let repo_dir = join_paths(&settings.project_base_path, &settings.repo_directory_name);

    run_rsync_or_copy(&tmp_repo_dir, &PathBuf::from(".bb"))?;

    if quick_backup.exists() {
        remove_dir_all(&quick_backup).context(format!(
            "{} could not be removed",
            style(&quick_backup.display()).bold()
        ))?;
    }

    if repo_dir.exists() {
        rename(&repo_dir, &quick_backup).context(format!(
            "{} could not be renamed to {}",
            style(&repo_dir.display()).bold(),
            style(&quick_backup.display()).bold()
        ))?;
    }

    // ensure repo dir is empty
    if repo_dir.exists() {
        remove_dir_all(&repo_dir).context(format!(
            "{} could not be removed",
            style(&repo_dir.display()).bold()
        ))?;
    }

    rename(&tmp_repo_dir, &repo_dir).context(format!(
        "{} could not be renamed to {}",
        style(&tmp_repo_dir.display()).bold(),
        style(&repo_dir.display()).bold()
    ))?;

    // ensure permissions are good on dest, make sure g+w is set
    bb_set_permissions(&repo_dir, true)?;

    hooks["post_restore"].run_hook()?;

    // cleanup
    remove_dir_all(&quick_backup)?;
    remove_dir_all(&quick_backup)?;

    Ok(())
}
