use std::{
    collections::HashMap,
    fs::{create_dir, read_to_string},
    path::{Path, PathBuf},
};

use anyhow::{Context, Ok, Result};

use crate::{
    helpers::{bb_set_permissions, message},
    hooks::{build_hooks, Hook},
    settings::SettingsSchema,
};

pub struct ValidatedReturn {
    pub settings: SettingsSchema,
    pub hooks: HashMap<&'static str, Hook>,
}

/**
 * validate the project configuration
 * and return the validated settings and defined hooks
 */
pub fn validate() -> Result<ValidatedReturn> {
    message(
        crate::helpers::CommandType::VALIDATE,
        String::from("beginning validation."),
    );

    // ensure bb.json exists in the current directory
    Path::new("./.bb.json")
        .try_exists()
        .context(".bb.json not found in path!")?;

    // validate the contents of bb.json against the expected schema
    let bb_config =
        read_to_string(Path::new("./.bb.json")).context("invalid JSON detected in .bb.json")?;
    let settings = serde_json::from_str::<SettingsSchema>(bb_config.as_str())
        .context("JSON does not match schema.")?;

    // validate the specified paths exist
    settings
        .project_base_path
        .try_exists()
        .context("user provided project base path was not found!")?;
    settings
        .backup_path
        .try_exists()
        .context("user provided backup path was not found!")?;

    // set the proper permissions on the backup_path and project_base_path directories
    bb_set_permissions(&settings.project_base_path, true)?;
    bb_set_permissions(&settings.backup_path, true)?;

    // create .bb directory if not exists
    if !Path::new(".bb").exists() {
        create_dir(".bb").context(
            "failed to generate .bb folder. Make sure your working directory has proper permissions."
        )?;
    }

    // set the proper permissions on the .bb directory
    bb_set_permissions(&PathBuf::from(".bb"), true)?;

    // init (or reinit) hooks
    let hooks = build_hooks().context("failed to build .bb/hooks")?;

    message(
        crate::helpers::CommandType::VALIDATE,
        String::from("end validation."),
    );

    Ok(ValidatedReturn { settings, hooks })
}
