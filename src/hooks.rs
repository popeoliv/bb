use std::{
    collections::HashMap,
    fs::{create_dir, File},
    io::Write,
    path::{Path, PathBuf},
    process::{Command, Output},
};

use anyhow::{Context, Ok, Result};
use console::style;

use crate::helpers::bb_set_permissions;

// Defines a hook object, which has a path field
pub struct Hook {
    pub path: PathBuf,
}

// Defines methods that can be implemented on a Hook object
pub trait HookFunctions {
    fn init_hook(&self) -> Result<()>;
    fn run_hook(&self) -> Result<Output>;
}

// Implementations of the HookFunctions trait for the Hook struct
impl HookFunctions for Hook {
    fn init_hook(&self) -> Result<()> {
        // create path if not exists
        if !self.path.exists() {
            let mut new_hook = File::create(&self.path)?;
            write!(
                new_hook,
                "#!/usr/bin/env bash\n# bb hook: {:?} \n",
                &self.path.as_os_str()
            )?;
        }

        // make sure path is executable
        bb_set_permissions(&self.path, true)?;

        Ok(())
    }
    fn run_hook(&self) -> Result<Output> {
        println!(
            "{}: {} RUNNING",
            style("hook").bold().cyan(),
            style(self.path.display()).blue()
        );
        let command = Command::new(&self.path).output().context(format!(
            "{} could not run, perhaps it doesn't exist?",
            style(&self.path.display()).bold().dim()
        ))?;

        Ok(command)
    }
}

// Builds a HashMap of hook objects with their corresponding hook names as keys
pub fn build_hooks() -> Result<HashMap<&'static str, Hook>> {
    let hooks = HashMap::from([
        // pre_backup
        (
            "pre_backup",
            Hook {
                path: PathBuf::from(".bb/hooks/pre-backup"),
            },
        ),
        // post_backup
        (
            "post_backup",
            Hook {
                path: PathBuf::from(".bb/hooks/post-backup"),
            },
        ),
        // pre_deploy
        (
            "pre_deploy",
            Hook {
                path: PathBuf::from(".bb/hooks/pre-deploy"),
            },
        ),
        // pre_pull
        (
            "pre_pull",
            Hook {
                path: PathBuf::from(".bb/hooks/pre-pull"),
            },
        ),
        // post_pull
        (
            "post_pull",
            Hook {
                path: PathBuf::from(".bb/hooks/post-pull"),
            },
        ),
        // build
        (
            "build",
            Hook {
                path: PathBuf::from(".bb/hooks/build"),
            },
        ),
        // pre_link
        (
            "pre_link",
            Hook {
                path: PathBuf::from(".bb/hooks/pre-link"),
            },
        ),
        // post_link
        (
            "post_link",
            Hook {
                path: PathBuf::from(".bb/hooks/post-link"),
            },
        ),
        // post_deploy
        (
            "post_deploy",
            Hook {
                path: PathBuf::from(".bb/hooks/post-deploy"),
            },
        ),
        // pre_restore
        (
            "pre_restore",
            Hook {
                path: PathBuf::from(".bb/hooks/pre-restore"),
            },
        ),
        // post_restore
        (
            "post_restore",
            Hook {
                path: PathBuf::from(".bb/hooks/post-restore"),
            },
        ),
    ]);

    // create hooks folder
    if !Path::new(".bb/hooks").exists() {
        create_dir(".bb/hooks")?;
    }
    bb_set_permissions(&PathBuf::from(".bb/hooks"), true)?;

    for (_key, ele) in &hooks {
        ele.init_hook()?;
    }

    Ok(hooks)
}
