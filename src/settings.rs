/**
 * define a struct to represent the settings schema and implement the required traits
 */

use schemars::JsonSchema;
use serde;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use url::Url;

#[derive(Serialize, Deserialize, Debug, JsonSchema)]
pub struct SettingsSchema {
    // the URL of the git repository for the project
    pub git_repository_url: Url,

    // the base path of the project on the local system
    pub project_base_path: PathBuf,

    // the name of the directory where the project's git repository is located
    pub repo_directory_name: PathBuf,

    // the path to the backup directory for the project
    pub backup_path: PathBuf,
}
