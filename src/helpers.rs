use anyhow::{Error, Ok, Result};
use console::{style, Style};
use faster_hex::hex_string;
use fs_extra::dir::CopyOptions;
use gzp::{
    deflate::Mgzip,
    par::{compress::ParCompressBuilder, decompress::ParDecompressBuilder},
};
use sha2::{Digest, Sha256};
use std::{
    ffi::OsString,
    fs::{create_dir, File},
    path::PathBuf,
    process::{Command, Stdio},
};
use tar::{Archive, Builder};
use which::which;

#[cfg(unix)]
use std::fs::{set_permissions, Permissions};

#[cfg(unix)]
use std::os::unix::prelude::PermissionsExt;

#[cfg(unix)]
use unix_mode;

/**
 * Join 2 paths into 1 path
 */
pub fn join_paths(a: &PathBuf, b: &PathBuf) -> PathBuf {
    [a, b].iter().collect()
}

/**
 * Create a directory if a path is nonexistent
 */
pub fn create_dir_if_not_exists(path: &PathBuf) -> Result<()> {
    if !path.exists() {
        create_dir(path)?;
    }
    Ok(())
}

pub enum CommandType {
    BACKUP,
    RESTORE,
    DEPLOY,
    VALIDATE,
}

pub fn message(command_type: CommandType, msg: String) {
    let styled_prefix = match command_type {
        CommandType::BACKUP => Style::new().bold().green().apply_to("backup"),
        CommandType::RESTORE => Style::new().bold().red().apply_to("restore"),
        CommandType::DEPLOY => Style::new().bold().blue().apply_to("deploy"),
        CommandType::VALIDATE => Style::new().bold().cyan().apply_to("validate"),
    };

    println!("{}: {}", styled_prefix, msg.as_str());
}

pub fn debug_message(msg: &str) {
    if std::env::var_os("VERBOSE_MODE").unwrap_or(OsString::from("0")) == "1" {
        println!("{}: {}", style("debug").dim().bold(), style(msg).dim());
    }
}

/**
 * This code attempts to run rsync to copy a directory from one location to another, and falls
 * back to using fs_extra's copy function if rsync is not available.
 * The function returns the exit code of the copy operation.
 */
pub fn run_rsync_or_copy(src: &PathBuf, dest: &PathBuf) -> Result<bool> {
    // Check if rsync is available in the system
    let binary_path = which("rsync");

    // If rsync is available, run it with the given arguments and return the exit code
    // Otherwise, use fs_extra's copy function to copy the source directory to the destination directory
    let exit_code = match binary_path {
        std::result::Result::Ok(a) => Command::new(a)
            .args([
                "-rlDP",
                "--info=progress2",
                src.to_str().unwrap(),
                dest.to_str().unwrap(),
            ])
            .stdout(Stdio::null())
            .spawn()?
            .wait()?
            .success(),
        Err(_) => {
            let options = CopyOptions::new();

            fs_extra::dir::copy(&src, &dest, &options)? > 0
        }
    };

    Ok(exit_code)
}

/**
 * calculates the SHA256 checksum of a given file and
 * returns the hexadecimal string representation of the hash
 * and the total size of the file.
 */
pub fn checksum(path: &PathBuf) -> Result<(String, u64), anyhow::Error> {
    // Open the given file
    let mut file = File::open(&path)?;

    // Create a Sha256 hasher
    let mut sha256 = Sha256::new();

    // Copy the file's contents into the hasher and get the total size of the file
    let total_size = std::io::copy(&mut file, &mut sha256)?;

    // Calculate the final hash of the file
    let result = sha256.finalize();

    // Return the hexadecimal string representation of the hash and the total size of the file
    return Ok((hex_string(&result[..]), total_size));
}

/**
 * This function creates a tar archive of the given input directory
 * and writes it to the specified output file.
 * It returns an error if any step in the process fails.
 */
pub fn tar(input: &PathBuf, output: &PathBuf) -> Result<(), Error> {
    // Create the output file
    let file = File::create(&output)?;

    // Create a tar archive builder that writes to the output file
    let mut archive = Builder::new(&file);

    // Add the contents of the input directory to the archive
    archive.append_dir_all(".", &input)?;

    // Finish building the archive and sync it to disk
    archive.finish()?;
    file.sync_all()?;

    Ok(())
}

/**
 * This function unpacks the given tar archive file and
 * extracts the contents to the specified output directory */
pub fn untar(input_file: &PathBuf, output_dir: &PathBuf) -> Result<()> {
    // Open the input file
    let file = File::open(&input_file)?;

    // Create a tar archive reader that reads from the input file
    let mut archive = Archive::new(file);

    // Extract the contents of the archive to the output directory
    archive.unpack(&output_dir)?;

    Ok(())
}

/**
 * This function uncompresses the given gzip compressed file and
 * writes the uncompressed data to the specified output file
 */
pub fn inflate(input: &PathBuf, output: &PathBuf) -> Result<u64, Error> {
    // Create a reader that decompresses the input file
    let mut rdr = ParDecompressBuilder::<Mgzip>::new()
        .num_threads(num_cpus::get())? // Use all available CPU threads
        .from_reader(File::open(&input)?);

    // Create the output file
    let mut wtr = File::create(&output)?;

    // Copy the decompressed data from the reader to the output file and return the total number of bytes written
    let bytes = std::io::copy(&mut rdr, &mut wtr)?;

    Ok(bytes)
}

/**
 * This function compresses the given input file
 * and writes the gzip compressed data to the specified output file
 */
pub fn deflate(input: &PathBuf, output: &PathBuf) -> Result<u64, Error> {
    // Open the input file
    let mut rdr = File::open(&input)?;

    // Create a writer that compresses the data using parallel compression
    let mut wtr = ParCompressBuilder::<Mgzip>::new()
        .num_threads(num_cpus::get())? // Use all available CPU threads
        .from_writer(File::create(&output)?);

    // Copy the data from the input file to the compressed output file and return the total number of bytes written
    let bytes = std::io::copy(&mut rdr, &mut wtr)?;

    Ok(bytes)
}

/**
 * This function creates a tar archive of the input directory,
 * generates a SHA256 checksum of the archive,
 * and then compresses the archive using gzip to the
 * specified output directory.
 * It returns the checksum and the total size of the
 * uncompressed and compressed files.
 */
pub fn tar_and_deflate(
    input_dir: &PathBuf,
    output_dir: &PathBuf,
    tmp_dir: &PathBuf,
) -> Result<(String, u64, u64)> {
    // create a temporary output file to work on before compressing
    let temp_file = join_paths(&tmp_dir, &PathBuf::from("tmp.tar"));

    // create a tar file from a given directory
    tar(&input_dir, &temp_file)?;

    // generate checksum
    let (checksum, checksum_bytes) = checksum(&temp_file)?;

    // use checksum to name the compressed tar file
    let output_file = join_paths(&output_dir, &PathBuf::from(format!("{}.tgz", checksum)));

    // compress tar file
    let compressed_bytes = deflate(&temp_file, &output_file)?;

    Ok((checksum, checksum_bytes, compressed_bytes))
}

/**
 * This function decompresses a gzip compressed tar archive and
 * extracts the contents to the specified output directory
 */
pub fn untar_and_inflate(
    input_file: &PathBuf,
    output_dir: &PathBuf,
    tmp_dir: &PathBuf,
) -> Result<(String, u64, u64)> {
    // Create a temporary output file to hold the decompressed tar data
    let temp_file = join_paths(&tmp_dir, &PathBuf::from("tmp.tar"));

    // Decompress the input file to the temporary output file and get the total number of bytes written
    let decompressed_bytes = inflate(&input_file, &temp_file)?;

    // Calculate the checksum of the temporary output file and get the total size of the file
    let (checksum, checksum_bytes) = checksum(&temp_file)?;

    // Extract the contents of the temporary output file to the specified output directory
    untar(&temp_file, &output_dir)?;

    Ok((checksum, checksum_bytes, decompressed_bytes))
}

/**
 * This function sets the permissions of the given file
 * or directory to the specified values, and if recurse is set to true,
 * it will also set the permissions of all files and directories
 * within the given directory
 */
pub fn bb_set_permissions(path: &PathBuf, recurse: bool) -> Result<()> {
    if cfg!(unix) { // Only run on Unix systems
        // Open the file or directory
        let file = File::open(path)?;

        // Set the desired permissions based on whether the path is a file or directory
        let wanted_permissions = Permissions::from_mode(if path.is_dir() {
            0o00040775
        } else {
            0o00100775
        });

        // If the file or directory does not have the desired permissions, set them
        if file.metadata()?.permissions().mode() != wanted_permissions.mode() {
            set_permissions(&path, wanted_permissions)?;

            debug_message(
                format!(
                    "permissions {}  {}",
                    style(unix_mode::to_string(file.metadata()?.permissions().mode())).bold(),
                    style(&path.display()).bold(),
                )
                .as_str(),
            );
        }

        /*
        If the path is a directory and recurse is set to true,
        set the permissions of all files and directories within it
        */
        if path.is_dir() && recurse {
            for file in walkdir::WalkDir::new(&path)
                .follow_links(true)
                .into_iter()
                .filter_map(|e| e.ok())
            {
                bb_set_permissions(&file.path().to_path_buf(), false)?;
            }
        }
    }

    Ok(())
}
