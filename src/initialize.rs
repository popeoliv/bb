use std::{path::{Path, PathBuf}, fs::File, io::Write};

use anyhow::{Result, Context};
use console::style;
use dialoguer::Input;
use url::Url;

use crate::{helpers::{message, CommandType}, settings::SettingsSchema};


/**
initialize the project with the specified settings, or ask for them if in interactive mode
*/
pub fn initialize(no_interactive: bool) -> Result<()> {
    if Path::new(".bb.json").exists() {
        message(
            CommandType::VALIDATE,
            String::from(".bb.json already exists. project already initialized?"),
        );
        return Ok(());
    }

    let mut file = File::create(Path::new(".bb.json"))?;

    // if the no_interactive parameter is true, create default settings
    if no_interactive {
        let default_settings = SettingsSchema {
            backup_path: PathBuf::from(""),
            project_base_path: PathBuf::from(""),
            repo_directory_name: PathBuf::from("repo"),
            git_repository_url: Url::parse("https://example.com")?,
        };

        // write the default settings to the file and return an error if it fails
        file.write_all(serde_json::to_string_pretty(&default_settings)?.as_bytes())?;
    }

    // ask user for values
    let git_repository_url: Url = Input::new()
        .with_prompt("git repository url")
        .interact_text()?;

    let project_base_string: String = Input::new()
        .with_prompt("project base path")
        .interact_text()?;
    let project_base_path = PathBuf::from(project_base_string);
    project_base_path.try_exists().context(format!(
        "The specified path, {}, doesn't exist!",
        project_base_path.display()
    ))?;

    let backup_base_string: String = Input::new()
        .with_prompt("backup base path")
        .interact_text()?;
    let backup_path = PathBuf::from(backup_base_string);
    backup_path.try_exists().context(format!(
        "The specified path, {}, doesn't exist!",
        backup_path.display()
    ))?;

    let repo_directory_name_string: String = Input::new()
        .with_prompt("project base path")
        .interact_text()?;
    let repo_directory_name = PathBuf::from(repo_directory_name_string);

    let new_settings = SettingsSchema {
        project_base_path,
        backup_path,
        git_repository_url,
        repo_directory_name,
    };

    // write the use provided settings to file.
    file.write_all(serde_json::to_string_pretty(&new_settings)?.as_bytes())?;

    message(CommandType::VALIDATE, format!("{} {}", style("systems nominal. ").bold().green(), style("continuing...").italic()));

    Ok(())

}
