#!/usr/bin/env bash
TARGETS=('aarch64-apple-darwin' 'x86_64-apple-darwin' 'aarch64-unknown-linux-musl' 'x86_64-unknown-linux-musl' 'x86_64-pc-windows-gnu')

# ensure rust is installed
if ! [ -x "$(command -v rustup)" ]; then
  echo 'Error: rust is not installed, please install the toolchain here: https://www.rust-lang.org/learn/get-started.' >&2
  exit 1
fi

# ensure brew is installed
if ! [ -x "$(command -v brew)" ]; then
  echo 'Error: brew is not installed, please install here: https://brew.sh/.' >&2
  exit 1
fi

# ensure mingw is installed for windows cross-compilation
if ! [ -x "$(command -v x86_64-w64-mingw32-gcc)" ]; then
  echo 'Error: x86_64-w64-mingw32-gcc is not installed. attempting to install with brew' >&2
  brew update
  brew install mingw-w64
fi

mkdir -p dist
for i in "${TARGETS[@]}"; do
    echo "building bb for target $i"
    rustup target add "$i"
    cargo build --release --target "$i"
    SUFFIX=""
    if [[ $i == "x86_64-pc-windows-gnu" ]]; then
        SUFFIX=".exe"
    fi

    tar -czvf "dist/bb-$i.tar.gz" -C "target/$i/release" "bb$SUFFIX"
    tar -cjvf "dist/bb-$i.tar.bz2" -C "target/$i/release" "bb$SUFFIX"
    tar -cJvf "dist/bb-$i.tar.xz" -C "target/$i/release" "bb$SUFFIX"
done
